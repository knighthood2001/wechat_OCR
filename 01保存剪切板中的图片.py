import PIL.ImageGrab
import time

# try:
#     img = PIL.ImageGrab.grabclipboard()
#     img.save(r"img\~~ocr.png")
#     print("剪切板中的图片已经保存")
# except AttributeError:
#     print("剪切板中不是图片")
# time.sleep(0.5)

def save_clipboard_pic(save_path):
    try:
        img = PIL.ImageGrab.grabclipboard()
        img.save(save_path)
        print("剪切板中的图片已经保存")
        return True
    except AttributeError:
        print("剪切板中不是图片")
        return False

print(save_clipboard_pic('111.png'))