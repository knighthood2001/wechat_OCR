import pyperclip

# 打开要读取的文本文件
file_path = "text_save.txt"  # 替换为你的文件路径
with open(file_path, "r", encoding="utf-8") as file:
    # 读取文件内容
    file_content = file.read()

# 将文件内容复制到剪贴板
pyperclip.copy(file_content)
print("已将文件内容复制到剪贴板。")

def txt_copy(file_path):
    with open(file_path, "r", encoding="utf-8") as file:
        # 读取文件内容
        file_content = file.read()
    # 将文件内容复制到剪贴板
    pyperclip.copy(file_content)
    print("已将文件内容复制到剪贴板。")