import os
import json
import time
from wechat_ocr.ocr_manager import OcrManager, OCR_MAX_TASK_ID

def ocr_result_callback(img_path: str, results: dict):
    result_file = os.path.join("json", os.path.basename(img_path) + ".json")  # json\01.png.json，这里是包含文件夹名称
    print(f"识别成功，img_path: {img_path}, result_file: {result_file}")
    with open(result_file, 'w', encoding='utf-8') as f:
        f.write(json.dumps(results, ensure_ascii=False, indent=2))

def save_to_json(img_file):
    ocr_manager = OcrManager(wechat_dir)
    # 设置WeChatOcr目录
    ocr_manager.SetExePath(wechat_ocr_dir)
    # 设置微信所在路径
    ocr_manager.SetUsrLibDir(wechat_dir)
    # 设置ocr识别结果的回调函数
    ocr_manager.SetOcrResultCallback(ocr_result_callback)
    # 启动ocr服务
    ocr_manager.StartWeChatOCR()
    # TODO 识别图片
    # 如果需要识别多张图片，可以多复制下面这一行代码，指定对应图片即可
    # ocr_manager.DoOCRTask(r"img\~~ocr.png")
    # ocr_manager.DoOCRTask(r"img\01.png")
    ocr_manager.DoOCRTask(img_file)

    time.sleep(1)
    while ocr_manager.m_task_id.qsize() != OCR_MAX_TASK_ID:
        pass
    # 识别输出结果
    ocr_manager.KillWeChatOCR()

def save_text(json_file, save_file, mode=1):
    # 打开 JSON 文件
    with open(json_file, 'r', encoding='utf-8') as file:
        # 从文件中加载 JSON 数据
        data = json.load(file)
    # 换行保存
    if mode == 1:
        with open(save_file, 'w', encoding='utf-8') as f:
            # 提取每个对象的 text 字段
            for item in data['ocrResult']:
                print(item['text'])
                f.write(item['text'] + '\n')
    # 不换行保存
    if mode == 2:
        with open(save_file, 'w', encoding='utf-8') as f:
            # 提取每个对象的 text 字段
            for item in data['ocrResult']:
                print(item['text'])
                f.write(item['text'])

if __name__ == '__main__':
    # TODO 更改你电脑WeChatOCR.exe路径
    wechat_ocr_dir = r"C:\Users\Lenovo\AppData\Roaming\Tencent\WeChat\XPlugin\Plugins\WeChatOCR\7079\extracted\WeChatOCR.exe"
    # TODO 更改你电脑mmmojo.dll所在路径
    wechat_dir = r"G:\applicationsoftware\WeChat\[3.9.10.19]"

    img_file = r"img\01.png"
    # 使用 os.path.basename() 函数获取文件名
    file_name = os.path.basename(img_file)

    json_file = os.path.join("json", file_name+".json")  # 通过上述操作将01.png变成json\01.png.json
    save_file = "text_save.txt"
    save_to_json(img_file)
    save_text(json_file, save_file, mode=1)