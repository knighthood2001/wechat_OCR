import win32api
import win32con
import time

# 定义变量用于判断是否按下了 Ctrl+C 和 Esc 键
ctrl_c_pressed = False
esc_pressed = False

try:
    print("按下 Ctrl+C 或 Esc 键来测试...")
    while not esc_pressed:
        # 获取键盘状态，检查 Ctrl+C 和 Esc 键是否被按下
        if win32api.GetAsyncKeyState(ord('C')) and win32api.GetAsyncKeyState(win32con.VK_CONTROL):
            ctrl_c_pressed = True
            print("Ctrl+C 被按下")


        # 检查 Esc 键是否被按下
        if win32api.GetAsyncKeyState(win32con.VK_ESCAPE):
            esc_pressed = True
            print("Esc 键被按下，准备退出...")
        time.sleep(0.1)  # 在此处可以执行其他任务
except KeyboardInterrupt:
    print("手动中断")

print("程序结束")


