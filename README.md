
## 配置环境 
首先创建虚拟环境
```shell
python -m venv venv
```
然后激活这个虚拟环境
```shell
venv\scripts\activate
```
---
然后如果你的pip版本不够高，使用这个命令升级一下
```shell
python -m pip install --upgrade pip
```
---
接下来安装kanadeblisst给我们写好的库
```shell
pip install wechat_ocr
```
## 具体操作
### 获取两个文件的路径
接着，你需要知道你电脑`WechatOCR.exe`的路径和`mmmojo.dll`的路径，可以使用everything软件进行快速查找。

比如我电脑的`WechatOCR.exe`路径为：

C:\Users\Lenovo\AppData\Roaming\Tencent\WeChat\XPlugin\Plugins\WeChatOCR\7079\extracted\WeChatOCR.exe

我电脑的`mmmojo.dll`路径为：

G:\applicationsoftware\WeChat\[3.9.10.19]

可以看到`WechatOCR.exe`一般在C盘中，而`mmmojo.dll`一般在你安装的微信具体版本中。

这里注意一下，`WechatOCR.exe`是需要具体到exe文件的，而`mmmojo.dll`只需要到微信具体版本即可。

## 新建两个文件夹
新建两个文件夹，一个是`img`文件夹，另一个是`json`文件夹，`img`文件夹中存放的是你要识别图片文字的图片，`json`文件夹是后续对图片进行文字ocr识别后，会产生json文件保存在这里。

## 代码

```python
import os
import json
import time
from wechat_ocr.ocr_manager import OcrManager, OCR_MAX_TASK_ID
# 第一步，获取以下两个内容的路径
# TODO 更改你电脑WeChatOCR.exe路径
wechat_ocr_dir = r"C:\Users\Lenovo\AppData\Roaming\Tencent\WeChat\XPlugin\Plugins\WeChatOCR\7079\extracted\WeChatOCR.exe"
# TODO 更改你电脑mmmojo.dll所在路径
wechat_dir = r"G:\applicationsoftware\WeChat\[3.9.10.19]"

def ocr_result_callback(img_path: str, results: dict):
    result_file = os.path.join("json", os.path.basename(img_path) + ".json")
    print(f"识别成功，img_path: {img_path}, result_file: {result_file}")
    with open(result_file, 'w', encoding='utf-8') as f:
        f.write(json.dumps(results, ensure_ascii=False, indent=2))

def save_to_json():
    ocr_manager = OcrManager(wechat_dir)
    # 设置WeChatOcr目录
    ocr_manager.SetExePath(wechat_ocr_dir)
    # 设置微信所在路径
    ocr_manager.SetUsrLibDir(wechat_dir)
    # 设置ocr识别结果的回调函数
    ocr_manager.SetOcrResultCallback(ocr_result_callback)
    # 启动ocr服务
    ocr_manager.StartWeChatOCR()
    # TODO 识别图片
    # 如果需要识别多张图片，可以多复制下面这一行代码，指定对应图片即可
    ocr_manager.DoOCRTask(r"img\~~ocr.png")
    # ocr_manager.DoOCRTask(r"img\2.png")
    
    time.sleep(1)
    while ocr_manager.m_task_id.qsize() != OCR_MAX_TASK_ID:
        pass
    # 识别输出结果
    ocr_manager.KillWeChatOCR()


if __name__ == "__main__":
    save_to_json()
```
操作完以上内容后，只需要复制我的代码，然后修改其中的图片地址即可。即可实现将图片中的文字ocr，将结果保存到json中。

注意，保存到json中是原作者写好的，我们只负责调用即可。

## 运行结果
代码架构如下：
![img.png](img.png)

原始图片如下为img目录下的`01.png`，图片如下
![01.png](img\01.png)

运行代码后，其会在json文件夹下面生成对应文件的json文件

```json
{
  "taskId": 1,
  "ocrResult": [
    {
      "text": "然后如果你的pip版本不够高，使用这个命令升级一下",
      "location": {
        "left": 16.858334,
        "top": 16.858334,
        "right": 424.92917,
        "bottom": 35.204166
      },
      "pos": {
        "x": 16.858334,
        "y": 16.858334
      }
    },
    {
      "text": "python -m pip install --upgrade pip",
      "location": {
        "left": 35.204166,
        "top": 84.291664,
        "right": 324.275,
        "bottom": 99.166664
      },
      "pos": {
        "x": 35.204166,
        "y": 84.291664
      }
    }
  ]
}
```
可以看到，其中"text"对应的内容就是OCR识别的文本。

下一篇文章，我会实现将其中的文本抽出来。
